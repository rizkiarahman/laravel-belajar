<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome">
        <label>First name:</label><br>
        <input type="text" name="namadepan"><br><br>
        
        <label>Last name:</label><br>
        <input type="text" name="namabelakang"><br><br>

        <label>Gender:</label><br>
        <input type="radio">Man<br>
        <input type="radio">Woman<br>
        <input type="radio">Other<br><br>

        <label for="nationality">Nationality:</label>
        <select name="nationality" id="nationality">
            <option value="Indonesian" selected="Indonesia">Indonesia</option>
            <option value="Singaporean">Singapore</option>
            <option value="Malaysian">Malaysia</option>
            <option value="Australian">Thailand</option>
        </select><br><br>

        <label for="languange">Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Arabic<br>
        <input type="checkbox">Japanese<br><br> 

        <label for="bio">Bio</label><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" name="submit" value="Sign Up">
        </br>
    </form>
</body>
</html>