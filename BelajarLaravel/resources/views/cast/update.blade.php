@extends('layout.master')
@section('judul')
  Halaman Update Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" value="{{old('nama',$cast->nama)}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur Cast</label>
    <input type="text" name="umur" value="{{old('umur',$cast->umur)}}" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Biodata Cast</label>
    <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{old('bio',$cast->bio)}}</textarea> 
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group form-check">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection