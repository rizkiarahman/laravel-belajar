<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'nama' => 'required|min:6',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Nama Cast tidak boleh kosong harus di isi',
                'nama.min' => 'Nama Cast Minima 6 Karakter',
                'umur.required' => 'Umur tidak boleh kosong harus di isi',
                'bio.required' => 'Bio tidak boleh kosong harus di isi',

            ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']

            ]
        );

        return redirect('/cast');
    }

    public function index()
    {

        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.update', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required|min:6',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Nama Cast tidak boleh kosong harus di isi',
                'nama.min' => 'Nama Cast Minima 6 Karakter',
                'umur.required' => 'Umur tidak boleh kosong harus di isi',
                'bio.required' => 'Bio tidak boleh kosong harus di isi',

            ]
        );

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
